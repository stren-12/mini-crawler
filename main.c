/**
 * mini-crawler
 * 
 * daemon to download urls to file in linux systems 
 * use it as solution to fetch data from the web 
 * @version 0.1
 * @author  Sultan Fahad Aljohani <smagsf@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT License
 * @link    https://gitlab.com/stren-12/mini-crawler
 */

/* System includes*/
#include <stdio.h>
#include <string.h> //strlen
#include <stdlib.h> //strlen
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <pthread.h>   //for threading , link with lpthread
#include <curl/curl.h>
#include <curl/easy.h>
#include <json-c/json.h>
#include <syslog.h>
#include <time.h>

/* Our includes */
/* the constants */
#include "setting.h"
#include "skeleton_daemon.h"
#include "mhash_functions.h"

/* 
* Error loging 
* this will be daemon so there is no stderr or stdout
* any error will be logged by syslog() 
* to see the levels see https://ftp.gnu.org/old-gnu/Manuals/glibc-2.2.3/html_chapter/libc_18.html (it's very old 2 May 2001)
*/

//the thread function

/* 
* for out-of-memory conditions just report the error and exit
* @see https://eli.thegreenplace.net/2009/10/30/handling-out-of-memory-conditions-in-c
* Note: on linux even if the ram is full, check the swap finaly run the oom killer
* On Linux, malloc() will never fail -- instead, the OOM killer will be triggered and begin killing random processes 
* until the system falls over. Since Linux is the most popular UNIX derivative in use today 
* many developers have learned to just never check the result of malloc().
* That's probably why your colleagues ignore malloc() failures 
* from:https://stackoverflow.com/questions/1439977/what-is-the-correct-way-to-handle-out-of-memory
* TL;DR: use linux.
*/
/* TODO use this function with syslog() */
static void oom(const char *msg)
{
    fprintf(stderr, "%s: Out of memory\n", msg);
    fflush(stderr);
    sleep(1);
    exit(1);
}

/*
* This will handle connection for each client
* 
*/
static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{

    size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
    return written;
}

static void add_transfer(CURLM *cm,char file_path,char *url,FILE *fp)
{
    CURL *eh = curl_easy_init();
    curl_easy_setopt(eh, CURLOPT_FAILONERROR, 1L);
    curl_easy_setopt(eh, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(eh, CURLOPT_WRITEDATA, fp);
    curl_easy_setopt(eh, CURLOPT_URL, url);
    curl_easy_setopt(eh, CURLOPT_PRIVATE,file_path);
    curl_multi_add_handle(cm, eh);
}



void *connection_handler(void *socket_desc)
{
    syslog(LOG_DEBUG, "connection_handler start");

    //Get the socket descriptor
    int sock = *(int *)socket_desc;
    int read_size;
    char client_message[BUFFER_SIZE];
    struct json_object *parsed_json;
    struct json_object *urls;
    struct json_object *url;
    size_t n_urls;
    time_t begin,end; // time_t is a datatype to store time values.

    //Receive a message from client

    while (recv(sock, client_message, BUFFER_SIZE, 0) > 0)
    {
        //continue;
        parsed_json = json_tokener_parse(client_message);
        /* json_tokener_parse will return null if parsing fails see https://stackoverflow.com/questions/37773273/valid-json-check-in-c-language */

        if (parsed_json == NULL)
        {
            syslog(LOG_WARNING, "bad json is coming..");
            /* just exit the thread */
            free(socket_desc);
            close(sock);
            pthread_exit(EXIT_FAILURE);
        }
        syslog(LOG_DEBUG, "good json");
        json_object_object_get_ex(parsed_json, "urls", &urls);
        n_urls = json_object_array_length(urls);
        int n_urls_int = (int)((ssize_t)n_urls); 
        char **urls_array = (char**) calloc(n_urls, sizeof(char*));
        int i;
        int len;
        for (i = 0; i < n_urls; i++)
        {
            url = json_object_array_get_idx(urls, i);
            len = strlen(json_object_get_string(url)) + 1; 
            urls_array[i] = (char*) calloc(len, sizeof(char));

            sprintf(urls_array[i], "%s", json_object_get_string(url));

        }
        syslog(LOG_DEBUG, "%d",n_urls);

        FILE *fp[n_urls];
        unsigned int transfers = 0;
        int msgs_left = -1;
        int still_alive = 1;
        CURLM *cm;
        CURLMsg *msg;
        time (&begin);
        curl_global_init(CURL_GLOBAL_ALL);
        cm = curl_multi_init();
        /* set some http header in order to get the best result (no 403 http header, arabic languge)*/ 
        /* from my php library https://github.com/stren-12/AppsScraper */
        struct curl_slist *chunk = NULL;
        /* TODO: move to setting.h */
        chunk = curl_slist_append(chunk, "Accept-Language: ar-SA,ar;q=0.9,en-US;q=0.8,en;q=0.7");
        chunk = curl_slist_append(chunk, "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36");
        chunk = curl_slist_append(chunk, "Upgrade-Insecure-Requests: 1");
        /* fake referer */
        chunk = curl_slist_append(chunk, "Referer: https://www.google.com/");

        /* TODO REAL CURLMOPT_MAXCONNECTS*/
        curl_multi_setopt(cm, CURLMOPT_MAXCONNECTS, (long)n_urls);
        for (i = 0; i < n_urls; i++)
        {
            char hash_string[64] = "";
            puts(urls_array[i]);

            calc_sha256(urls_array[i], &hash_string);
            /* TODO: use some safer function then strcat */

            char *fullpath;
            asprintf(&fullpath,"%s%s",STORAGE_PATH,hash_string);

            char *log_msg;
            asprintf(&log_msg,"url %s file %s",urls_array[i],fullpath);
            syslog(LOG_DEBUG,"%s",log_msg);

           

            fp[i] = fopen(fullpath, "w");
            if (fp == NULL)
            {
                syslog(LOG_WARNING, "Could not open file %s", fullpath);
                continue;
            }
            add_transfer(cm,fullpath, urls_array[i],fp[i]);
        }
         /* https://curl.haxx.se/libcurl/c/10-at-a-time.html */
        do
        {
            curl_multi_perform(cm, &still_alive);

            while ((msg = curl_multi_info_read(cm, &msgs_left)))
            {
                if (msg->msg == CURLMSG_DONE)
                {
                    long http_code = -1;
                    char *file_path;
                    char *CURL_url;
                    CURL *e = msg->easy_handle;
                    curl_easy_getinfo(msg->easy_handle, CURLINFO_PRIVATE, &file_path);
                    curl_easy_getinfo(msg->easy_handle, CURLINFO_EFFECTIVE_URL, &CURL_url);
                    curl_easy_getinfo(msg->easy_handle, CURLINFO_RESPONSE_CODE, &http_code);
                    /* By Sultan: lets check if CURLE_OK (e.i. no time out error or network error) and http code must be 200*/
                    char *curl_msg;
                    asprintf(&curl_msg,"R: %d - %s HTTP code %ld <%s>\n",msg->data.result, curl_easy_strerror(msg->data.result),http_code, CURL_url);
                    if(msg->data.result !=  CURLE_OK || http_code != 200){
                        /* remove the un-wanted file */
                        unlink(file_path);
                    }
                 
                    syslog(LOG_INFO,curl_msg);
                    curl_multi_remove_handle(cm, e);
                    curl_easy_cleanup(e);
                }
                else
                {
                    syslog(LOG_WARNING, "Could not open file %s", msg->msg);
                }
            }
           // printf("The integer is: %d\n", still_alive);
            if (still_alive)
                curl_multi_wait(cm, NULL, 0, 1000, NULL);

        } while (still_alive);
        time (&end);
        double time = difftime (end,begin);
        syslog(LOG_INFO,"Elapsed: %.2lf seconds:",time);

        /* Note: this is daemon YOU MUST CLEAN UP AND FREE THE MEMORY OR YOU WILL HAVE MEMORY LEAK */
        for (i = 0; i < n_urls; i++)
        {
            free(urls_array[i]);
        }
        free(urls_array);
        
        curl_slist_free_all(chunk);
        curl_multi_cleanup(cm);
        curl_global_cleanup();
        pthread_exit(EXIT_SUCCESS);
       
    }
    

    //Free the socket pointer
    free(socket_desc);
    close(sock);
    pthread_exit(EXIT_SUCCESS);

    return 0;
}
int main(int argc, char *argv[])
{
    skeleton_daemon();
    syslog(LOG_NOTICE, "%s started with pid: %d", DAEMON_NAME, getpid());

    int socket_desc, client_sock, c, *new_sock;
    struct sockaddr_in server, client;

    //Create socket
    int option = 1;

    socket_desc = socket(AF_INET, SOCK_STREAM, 0);

    /* this to fix bind() failed: Address already in use */
    setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    if (socket_desc == -1)
    {
        syslog(LOG_ERR, "Could not create socket");
    }

    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);

    //Bind
    if (bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        //print the error message
        syslog(LOG_ERR, "bind failed. Error");
        return 1;
    }

    //Listen
    listen(socket_desc, 3);

    //Accept and incoming connection
    c = sizeof(struct sockaddr_in);

    while ((client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t *)&c)))
    {

        pthread_t sniffer_thread;
        new_sock = malloc(1);
        *new_sock = client_sock;

        if (pthread_create(&sniffer_thread, NULL, connection_handler, (void *)new_sock) < 0)
        {
            syslog(LOG_ERR, "could not create thread");
            return 1;
        }

        //Now join the thread , so that we dont terminate before the thread
        //pthread_join(sniffer_thread, NULL);
    }
    syslog(LOG_DEBUG, "good bye");

    if (client_sock < 0)
    {
        syslog(LOG_ERR, "accept failed");
        return 1;
    }

    return 0;
}
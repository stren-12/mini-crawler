/*
	C ECHO client example using sockets
*/
#include <stdio.h>      //printf
#include <string.h>     //strlen
#include <sys/socket.h> //socket
#include <arpa/inet.h>  //inet_addr
#include <unistd.h>

int main(int argc, char *argv[])
{
    int sock;
    struct sockaddr_in server;
    char server_reply[8192];

    //Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;
    server.sin_port = htons(5000);

    //Connect to remote server
    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
    char buffer[8192];
    FILE *fp;

    fp = fopen("urls.json", "r");
    fread(buffer, 8192, 1, fp);
    fclose(fp);

    puts("Connected\n");

    //keep communicating with server
    while (1)
    {
        //printf("Enter message : ");
        //scanf("%s", message);

        //Send some data
        if (send(sock, buffer, strlen(buffer), 0) < 0)
        {
            puts("Send failed");
            return 1;
        }
        break;

        //Receive a reply from the server
        if (recv(sock, server_reply, 8192, 0) < 0)
        {
            puts("recv failed");
            break;
        }

        puts("Server reply :");
        puts(server_reply);
    }

    close(sock);
    return 0;
}
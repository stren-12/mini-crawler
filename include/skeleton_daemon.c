/* this daemon code from https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux */ 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include "setting.h"
void skeleton_daemon()
{
    pid_t pid;

    /* Fork off the parent process */
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);

    /* On success: The child process becomes session leader */
    if (setsid() < 0)
        exit(EXIT_FAILURE);

    /* Catch, ignore and handle signals */
    //TODO: Implement a working signal handler */
    signal(SIGCHLD, SIG_IGN);
    signal(SIGHUP, SIG_IGN);

    /* Fork off for the second time*/
    pid = fork();

    /* An error occurred */
    if (pid < 0)
        exit(EXIT_FAILURE);

    /* Success: Let the parent terminate */
    if (pid > 0)
        exit(EXIT_SUCCESS);
    /* Set new file permissions */
    
    /* change by Sultan (from umask(0) )it's better to use 027 to make => 750 file permission 
    * NOTE: when play with umask in c add 0 to the left to tell the compiler it is an octal constant.
    * but in bash you just give 027 (Decimal)
    */
    umask(0027);


    /* Open the log file */
    openlog (DAEMON_NAME, LOG_PID, LOG_DAEMON);
}
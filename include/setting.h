#ifndef SETTING_H_INCLUDED
#define SETTING_H_INCLUDED


/* 
* Why we have 4096 for buffer size ?
* due to file system block size (in linux it's almost 4096 everywhare i.e ext4, xfs)
* we want just one loop for recv(2) 
*/
#define BUFFER_SIZE 8192

#define DAEMON_NAME "mini-crawler"

/* TODO: move to .conf file*/

#define PORT 5000
#define MAX_PARALLEL 12  
/* WITH trailing slash */

#define STORAGE_PATH "/home/st1rt123/سطح المكتب/my stuff/git repos/mini-crawler/tmp/"

#endif

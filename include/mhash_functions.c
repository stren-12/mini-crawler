#include <stdint.h>
#include <mhash.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strlen

const char  *calc_sha256(char * text,char* hash_string){
    int i;
    MHASH td;
    char string_buffer[3] = {};

    unsigned char buffer;
    unsigned char hash[32]; /* enough size for MD5 */

    td = mhash_init(MHASH_SHA256);

    if (td == MHASH_FAILED)
        return NULL;

    /*
    https://linux.die.net/man/3/mhash
    while (fread(&buffer, 1, 1, stdin) == 1)
    {
        mhash(td, &buffer, 1);
    } 
    */
    int len = strlen(text);
    for (i = 0; i < len; i++)
    {
        buffer = text[i];
        mhash(td,&buffer,1);
    }

    mhash_deinit(td, hash);

    for (i = 0; i < mhash_get_block_size(MHASH_SHA256); i++)
    {
        // 3 = 1 for "\0" and two for hash part
        sprintf(&string_buffer, "%.2x", hash[i]);
        /* TODO: use some safer function then strcat */
        strcat(hash_string,string_buffer);
        /*asprintf(&hash_string,"%s",string_buffer); */
    }
    return hash_string;
}